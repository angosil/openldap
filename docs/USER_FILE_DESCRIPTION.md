## Ldiff user file

I add a ldif file to add a user to the ldap server.   

```ldif
# Add user 1 in the Users OU for Authentication OU
dn: cn=User1,ou=Users,ou=Automation,dc=example,dc=org
objectClass: top
objectClass: person
objectClass: organizationalPerson
objectClass: inetOrgPerson
cn: User1
sn: SurName
mail: user1@example.org
uid: user1
userPassword: password1
```
 the description of the file is:
 
* **dn**: the **distinguished name** of the entry. It is the unique identifier of the entry in the directory. It is 
composed of the relative distinguished name (RDN) of the entry and the parent entry. In this case, the RDN is cn=User1 and the parent entry is ou=Users,ou=Automation,dc=example,dc=org.
* **objectClass**: the object class of the entry. It defines the type of the entry. In this case, the entry is of type person, organizationalPerson, and inetOrgPerson.
* **cn**: the common name of the entry. It is the name of the entry.
* **sn**: the surname of the entry. It is the last name of the entry.
* **mail**: the email address of the entry.
* **uid**: the user identifier of the entry. It is the username of the entry.
* **userPassword**: the password of the entry. It is the password of the entry.