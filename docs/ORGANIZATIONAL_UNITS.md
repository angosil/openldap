### Organisational unis ldif file description

I add a ldif file to add a tree ou to the ldap server.   

```ldif
# Add the OU Automation
dn: ou=Automation,dc=example,dc=org
objectClass: organizationalUnit
ou: Automation

# Add the OUs inside de Automation
dn: ou=Users,ou=Automation,dc=example,dc=org
objectClass: organizationalUnit
ou: Users

dn: ou=Developers,ou=Automation,dc=example,dc=org
objectClass: organizationalUnit
ou: Developers

dn: ou=Admins,ou=Automation,dc=example,dc=org
objectClass: organizationalUnit
ou: Admins

dn: ou=Roots,ou=Automation,dc=example,dc=org
objectClass: organizationalUnit
ou: Roots
```
 the description of the file is:
 
I add five organisational units to the ldap server. The first ou is the root ou. The other four ou are inside the root 
ou. The ou are: Automation, Users, Developers, Admins and Roots. The ou Users, Developers, Admins and Roots are inside 
the ou Automation.

* **dn**: the **distinguished name** of the entry. It is the unique identifier of the entry in the directory. In this 
case, the RDN is ou=Automation and the parent entry is dc=example,dc=org.
* **objectClass**: the object class of the entry. It defines the type of the entry. In this case, the entry is of type 
```organizationalUnit```.
* **ou**: the name of the entry. It is the name of the entry.
