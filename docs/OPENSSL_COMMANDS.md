## Openssl commands

### Generate a self signed certificate

first yo need to create a private key for a certificate authority (CA)
```bash
$ openssl genpkey -algorithm RSA -out ldap/certs/openldapCA.key
```
then you need to create a certificate for the CA
```bash
$ openssl req -x509 -new -key ldap/certs/openldapCA.key -out ldap/certs/openldapCA.crt
```
then you need to create a private key for the openldap server
```bash
$ openssl genpkey -algorithm RSA -out ldap/certs/openldap.key
```
then you need to create a certificate for the openldap server
```bash
$ openssl req -new -key ldap/certs/openldap.key -out ldap/certs/openldap.csr
```
then you need to sign the certificate with the CA
```bash
$ openssl x509 -req -days 3650 -in ldap/certs/openldap.csr -CA ldap/certs/openldapCA.crt -CAkey ldap/certs/openldapCA.key -out ldap/certs/openldap.crt
```
verify the ssl certificate of the openldap server
```bash
openssl s_client -connect openldap:1636
```
