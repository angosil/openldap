# openldap - README.md

## Description

This project is to set up and configures OpenLDAP and interact with a python application. I use docker and docker-comnpose 
to create the containers.  

## Setup

To start the containers, you can use the following command:

```bash
docker-compose up --build -d
```

### Setting up TLS

To use a secure connection and verify the server, I use a self-signed certificate. You can find more information about 
the creation of the certificate here: [OpenSSL Commands](docs/OPENSSL_COMMANDS.md)

## Usage

I use two container one for openldap and other for the application. The application container is to test the ldap server. 
For the openldap container, I use the bitnami image. This image is easy to use and configure. You can find more 
information here: [bitnami/openldap](https://hub.docker.com/r/bitnami/openldap). 

### Verify the openldap server

To verify the openldap server, you can use the following command:

```bash
docker-compose exec openldap bash
```
ones inside the container, you can use the ldapsearch command to verify the server.
```bash
$ ldapsearch -x -H "ldap://openldap:1389" -D "cn=admin,dc=example,dc=org" -W
Enter LDAP Password:
# extended LDIF
#
# LDAPv3
# base <> (default) with scope subtree
# filter: (objectclass=*)
# requesting: ALL
#

# search result
search: 2
result: 32 No such object

# numResponses: 1
```

The OpenLDAP server is empty. To add data to the server, you can use the ldif files. I create a folder and a volume in 
the docker-compose.yml file to share the ldif files between the host and the container. The folder is ```.ldap/share``` 
and the volume is ```.ldap/share:/home/ldap/share```. If you want to share the data between the host and the container, 
yo can use the folder ```.ldap/share```. 

### Adding a organization unit (ou)

I prepare a ldif file to add ou to the ldap server. You can find more information about this ldif files here: [Organization Units](docs/ORGANIZATIONAL_UNITS.md)
To add this script to the ldap server, you can use the following command:
```bash
$ ldapadd -x -H "ldap://openldap:1389"  -D "cn=admin,dc=example,dc=org" -W -f /home/ldap/share/oraganization_units.ldif
```

if not error occurs, you can see the new ou in the ldap server:
We can find the new OUs in the ldap server:

```bash
$ ldapsearch -x -H "ldap://openldap:1389" -D "cn=admin,dc=example,dc=org" -W -b "dc=example,dc=org" -s one "(objectClass=organizationalUnit)"
Enter LDAP Password:
# extended LDIF
#
# LDAPv3
# base <dc=example,dc=org> with scope oneLevel
# filter: (objectClass=organizationalUnit)
# requesting: ALL
#

# users, example.org
dn: ou=users,dc=example,dc=org
objectClass: organizationalUnit
ou: users

# Automation, example.org
dn: ou=Automation,dc=example,dc=org
objectClass: organizationalUnit
ou: Automation

# search result
search: 2
result: 0 Success

# numResponses: 3
# numEntries: 2
```

### Adding a users to the ldap server

To adding users to the ldap server, I create a ldif file. You can find more information about this ldif files here: [User File](docs/USER_FILE_DESCRIPTION.md)
to add the users to the ldap server, you can use the following command:

```bash
$ ldapadd -x -H "ldap://openldap:1389"  -D "cn=admin,dc=example,dc=org" -W -f /home/ldap/share/user02.ldif
Enter LDAP Password:
adding new entry "cn=User1,ou=Users,ou=Automation,dc=example,dc=org"

adding new entry "cn=User2,ou=Developers,ou=Automation,dc=example,dc=org"

adding new entry "cn=User3,ou=Admins,ou=Automation,dc=example,dc=org"

adding new entry "cn=User4,ou=Roots,ou=Automation,dc=example,dc=org"
```

to search the users in the ldap server, you can use the following command:

```bash
$ ldapsearch -x -H "ldap://openldap:1389" -D "cn=admin,dc=example,dc=org" -W -b "dc=example,dc
=org" -s sub "(objectClass=inetOrgPerson)"
Enter LDAP Password:
# extended LDIF
#
# LDAPv3
# base <dc=example,dc=org> with scope subtree
# filter: (objectClass=inetOrgPerson)
# requesting: ALL
#

# user01, users, example.org
dn: cn=user01,ou=users,dc=example,dc=org
cn: User1
cn: user01
sn: Bar1
objectClass: inetOrgPerson
objectClass: posixAccount
objectClass: shadowAccount
userPassword:: cGFzc3dvcmQx
uid: user01
uidNumber: 1000
gidNumber: 1000
homeDirectory: /home/user01

# user02, users, example.org
dn: cn=user02,ou=users,dc=example,dc=org
cn: User2
cn: user02
sn: Bar2
objectClass: inetOrgPerson
objectClass: posixAccount
objectClass: shadowAccount
userPassword:: cGFzc3dvcmQy
uid: user02
uidNumber: 1001
gidNumber: 1001
homeDirectory: /home/user02

# User1, Users, Automation, example.org
dn: cn=User1,ou=Users,ou=Automation,dc=example,dc=org
objectClass: top
objectClass: person
objectClass: organizationalPerson
objectClass: inetOrgPerson
cn: User1
sn: Apellido1
mail: user1@example.org
uid: user1
userPassword:: Y29udHJhc2XDsWEx

# User2, Developers, Automation, example.org
dn: cn=User2,ou=Developers,ou=Automation,dc=example,dc=org
objectClass: top
objectClass: person
objectClass: organizationalPerson
objectClass: inetOrgPerson
cn: User2
sn: Apellido2
mail: user2@example.org
uid: user2
userPassword:: Y29udHJhc2XDsWEy

# User3, Admins, Automation, example.org
dn: cn=User3,ou=Admins,ou=Automation,dc=example,dc=org
objectClass: top
objectClass: person
objectClass: organizationalPerson
objectClass: inetOrgPerson
cn: User3
sn: Apellido3
mail: user3@example.org
uid: user3
userPassword:: Y29udHJhc2XDsWEz

# User4, Roots, Automation, example.org
dn: cn=User4,ou=Roots,ou=Automation,dc=example,dc=org
objectClass: top
objectClass: person
objectClass: organizationalPerson
objectClass: inetOrgPerson
cn: User4
sn: Apellido4
mail: user4@example.org
uid: user4
userPassword:: Y29udHJhc2XDsWE0

# search result
search: 2
result: 0 Success

# numResponses: 7
# numEntries: 6
```

### Use python script to login (bind) users

This part is important because is the equivalent to our application. In this case I use a python script to login. This 
script use ldap3 library. And use a Dockerfile to create a container with the python-ldap library installed.

To test the script you can use the following command:
```bash
docker-compose exec application bash
```
and inside the container
```bash
$ python login.py <username> <password>
```
if the user and password are correct, the script return the user information. If not, the script return a error message.
