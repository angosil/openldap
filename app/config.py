import os
from typing import List


def format_host(hosts: str) -> List[List]:
    if not hosts:
        return []
    pairs = hosts.split(',')
    return [pair.split(':') for pair in pairs]


def to_boolean(value: str) -> bool:
    if value.lower() in ['true', '1', 'yes', 'y', 'on']:
        return True
    return False


class Config:
    # LDAP Server configuration
    LDAP_LABEL = os.getenv("LDAP_LABEL", "my server")
    LDAP_SERVER = os.getenv("LDAP_SERVER", "localhost")
    LDAP_PORT = int(os.getenv("LDAP_PORT", 636))
    LDAP_SEARCH_BASE_DN = os.getenv("LDAP_SEARCH_BASE_DN", "DC=example,DC=org")
    LDAP_ENCRYPTION = os.getenv(
        "LDAP_ENCRYPTION",
        "PLAIN"
    )  # the possibles values are: "PLAIN", "SIMPLE_TLS", "START_TLS"
    LDAP_USE_SSL = to_boolean(os.getenv("LDAP_USE_SSL", "false"))  # Set to True to use SSL

    LDAP_HOSTS = format_host(os.getenv("LDAP_HOSTS",
                                       ""))  # Array of LDAP [server, port]. If empty, the LDAP_SERVER will be used if not empty the LDAP_SERVER will be ignored
    LDAP_CERTIFICATE_VALIDATION = to_boolean(
        os.getenv("LDAP_CERTIFICATE_VALIDATION", "false"))  # Set to True to validate the server certificate
    LDAP_CONNECTION_TIMEOUT = int(os.getenv("LDAP_CONNECTION_TIMEOUT", 20))  # Connection timeout in seconds
    LDAP_IS_ACTIVE_DIRECTORY = to_boolean(
        os.getenv("LDAP_IS_ACTIVE_DIRECTORY", "false"))  # Set to True if the LDAP server is an Active Directory server

    # Search user credentials
    LDAP_SEARCH_DN = os.getenv("LDAP_SEARCH_DN", "cn=admin,dc=example,dc=org")
    LDAP_SEARCH_PASSWORD = os.getenv("LDAP_SEARCH_PASSWORD", "adminpassword")

    # Users configuration
    LDAP_USER_LOGIN_ATTR = os.getenv(
        "LDAP_USER_LOGIN_ATTR")  # Example: 'sAMAccountName' or 'uid' or 'userPrincipalName' or None
    LDAP_ALLOW_USERNAME_OR_EMAIL_LOGIN = to_boolean(os.getenv("LDAP_ALLOW_USERNAME_OR_EMAIL_LOGIN",
                                                        "false"))  # Set to True to allow login with username or email
    LDAP_USER_MAIL_ATTR = os.getenv("LDAP_USER_MAIL_ATTR")  # Example: 'mail' or 'userPrincipalName' or None
