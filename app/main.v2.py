import sys

from ldap3 import Server, Connection, ALL, SUBTREE, AUTO_BIND_NO_TLS

# LDAP server configuration
LDAP_SERVER = "192.168.1.153"  # 192.168.30.80
# LDAP_PORT = 1389 # 389
LDAP_PORT = 1636  # 636
LDAP_BASE_DN = "dc=example,dc=org"  # DC=bouyguestelecom,DC=fr

# admin credentials
LDAP_SEARCH_USERNAME = "admin"  # Admnistrator
LDAP_SEARCH_PASSWORD = "adminpassword"  # P@ssw0rd

# users configuration
LDAP_SEARCH_GROUPS = ["ou=Automation"]  # OU=ACS Authenticated Users
LDAP_OU_ROLE_MAP = ["Users:Automation User", "Developers", "Admins", "Roots"]
LDAP_USER_LOGIN_ATTR = "uid"
LDAP_USER_ATTRIBUTES = ['cn', 'sn', 'mail', 'uid']

# set up vars
ldap_server = Server(LDAP_SERVER, port=LDAP_PORT, use_ssl=True, get_info=ALL)
admin_dn = f"cn={LDAP_SEARCH_USERNAME},{LDAP_BASE_DN}"
search_base = ",".join(LDAP_SEARCH_GROUPS) + "," + LDAP_BASE_DN


def login(user_login: str, password: str):
    """Login user

    Args:
        user_login (str): Username or email
        password (str): Password

    Returns:
        bool: True if the user is authenticated, False otherwise
    """
    try:
        print(f"Connecting to LDAP server {ldap_server.host}")
        conn = Connection(ldap_server, user=admin_dn, password=LDAP_SEARCH_PASSWORD, auto_bind=AUTO_BIND_NO_TLS)

        search_filter = f"({LDAP_USER_LOGIN_ATTR}={user_login})"
        conn.search(search_base, search_filter, SUBTREE, attributes=LDAP_USER_ATTRIBUTES)

        if not conn.entries:
            print(f"User {user_login} not found in {conn.entries}")
            return

        print(type(conn.entries[0]))
        user_dn = conn.entries[0].entry_dn

        conn.user = user_dn  # Set the user DN for authentication
        conn.password = password
        if conn.bind():
            print(f"User {user_login} authenticated successfully")
        else:
            print(f"User {user_login} authentication failed")
            return

        for ou in LDAP_OU_ROLE_MAP:
            ou_dn = f"ou={ou},ou=Automation,dc=example,dc=org"
            conn.search(ou_dn, search_filter, SUBTREE, attributes=LDAP_USER_ATTRIBUTES)
            if conn.entries:
                print(f"User {user_login} belongs to {ou}")
                break
        else:
            print(f"User {user_login} does not belong to any of the specified OUs")

        print(f"User dn:{user_dn}")
        for attr in LDAP_USER_ATTRIBUTES:
            print(f"User attributes:{attr}:{conn.entries[0][attr]}")

    except Exception as e:
        print("Authentication error:", e)

        print("Error checking group membership:", e)
        return False


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python main.py <username> <password>")
    else:
        user, password = sys.argv[1:]
        print(f"Trying to login user {user}")
        login(user, password)
