import logging
import sys

from ldap3 import Connection, SUBTREE, ALL_ATTRIBUTES
from ldap3.utils.log import set_library_log_detail_level, EXTENDED

from builders import BuildFilter, BuildServers, BuildAutoBind
from config import Config

root = logging.getLogger()
root.setLevel(logging.DEBUG)

# handler = logging.StreamHandler(sys.stdout)
handler = logging.FileHandler('debug.log')
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
root.addHandler(handler)

set_library_log_detail_level(EXTENDED)


def login(user_login: str, user_password: str):
    ldap_servers = BuildServers().build()
    auto_bind = BuildAutoBind().build()
    print(f"Try to connect to LDAP / AD servers {ldap_servers}")
    conn = Connection(ldap_servers, user=Config.LDAP_SEARCH_DN, password=Config.LDAP_SEARCH_PASSWORD,
                      auto_bind=auto_bind)

    print(f"Find user {user_login}")
    search_filter = BuildFilter().build(user_login)
    print(f"Search filter: {search_filter}")
    conn.search(Config.LDAP_SEARCH_BASE_DN, search_filter, SUBTREE, attributes=ALL_ATTRIBUTES)

    if not conn.entries:
        print(f"User {user_login} not found in {conn.entries}")
        return

    print(type(conn.entries[0]))
    user_dn = conn.entries[0].entry_dn
    print(f"User dn:{user_dn}")

    conn.user = user_dn
    conn.password = user_password
    if conn.bind():
        print(f"User {user_login} authenticated successfully")
    else:
        print(f"User {user_login} authentication failed")
        return

    conn.search(Config.LDAP_SEARCH_BASE_DN, search_filter, SUBTREE, attributes=ALL_ATTRIBUTES)

    user_json = conn.entries[0].entry_attributes_as_dict
    print(f"User attributes:{user_json}")
    print(f"User {user_login} role: {user_json['memberOf']}")



if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python main.py <username> <password>")
        sys.exit(1)
    else:
        user, password = sys.argv[1:]
        print(f"Attempting to authenticate user {user}")
        login(user, password)
