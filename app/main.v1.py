import sys

import ldap

# LDAP server configuration
LDAP_SERVER = "openldap"
# LDAP_PORT = 1389
LDAP_PORT = 1636
LDAP_BASE_DN = "dc=example,dc=org"

# admin credentials
LDAP_SEARCH_USERNAME = "admin"
LDAP_SEARCH_PASSWORD = "adminpassword"

# users configuration
LDAP_SEARCH_GROUPS = ["ou=Automation"]
LDAP_OU_ROLE_MAP = ["Users", "Developers", "Admins", "Roots"]
LDAP_USER_LOGIN_ATTR = "uid"

# set up vars
ldap_server = f"ldaps://{LDAP_SERVER}:{LDAP_PORT}"

admin_dn = f"cn={LDAP_SEARCH_USERNAME},{LDAP_BASE_DN}"
search_base = f"{','.join(LDAP_SEARCH_GROUPS)},{LDAP_BASE_DN}"

ldap.set_option(ldap.OPT_DEBUG_LEVEL, 255)
cert_file = 'openldapCA.crt'
ldap.set_option(ldap.OPT_X_TLS_CACERTFILE, cert_file)


def login(user_login: str, password: str):
    """Login user

    Args:
        user_login (str): Username or email
        password (str): Password

    Returns:
        bool: True if the user is authenticated, False otherwise
    """
    try:
        # Establish TLS LDAP connection
        print(f"Connecting to LDAP server {ldap_server}")
        conn = ldap.initialize(ldap_server)
        # conn.set_option(ldap.OPT_X_TLS_PROTOCOL_MIN, ldap.OPT_X_TLS_PROTOCOL_TLS1_3)
        # conn.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_NEVER)
        conn.set_option(ldap.OPT_X_TLS_NEWCTX, 0)

        # Init TLS connection
        conn.start_tls_s()

        conn.simple_bind_s(admin_dn, LDAP_SEARCH_PASSWORD)
        print("Successfully connected to LDAP server")

        # search for the user by username
        search_filter = f"({LDAP_USER_LOGIN_ATTR}={user_login})"

        result = conn.search_s(search_base, ldap.SCOPE_SUBTREE, search_filter)
        print(f"User found {result}")

        if result is None or len(result) == 0:
            print(f"User {user_login} not found")
            return

        user_dn = result[0][0]

        # password verification
        if conn.simple_bind_s(user_dn, password):
            print(f"User {user_login} authenticated successfully")
        else:
            print(f"User {user_login} authentication failed")

        for ou in LDAP_OU_ROLE_MAP:
            ou_dn = f"ou={ou},ou=Automation,dc=example,dc=org"
            if ou_dn in user_dn:
                print(f"El usuario {user_login} pertenece a la OU: {ou}")
                break
        else:
            print(f"El usuario {user_login} no pertenece a ninguna de las OUs especificadas")

    except ldap.LDAPError as e:
        print("Authentication error:", e)
    finally:
        conn.unbind()


if __name__ == "__main__":
    user, password = sys.argv[1:]
    print(f"Trying to login user {user}")
    login(user, password)
