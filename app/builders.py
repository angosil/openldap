from ssl import CERT_REQUIRED, CERT_NONE

from ldap3 import Tls, Server, ALL, AUTO_BIND_NO_TLS, AUTO_BIND_TLS_BEFORE_BIND, AUTO_BIND_TLS_AFTER_BIND

from config import Config


class BuildServer:
    def __init__(self, host, port):
        self.host = host
        self.port = port
        use_ssl = str(Config.LDAP_PORT)[-3:] == '636' or Config.LDAP_ENCRYPTION.upper() in ["SIMPLE_TLS", "START_TLS"]
        self.use_ssl = Config.LDAP_USE_SSL if Config.LDAP_USE_SSL else use_ssl
        validate = CERT_REQUIRED if Config.LDAP_CERTIFICATE_VALIDATION else CERT_NONE
        self.tls = Tls(validate=validate)
        self.connect_timeout = Config.LDAP_CONNECTION_TIMEOUT

    def build(self):
        return Server(
            self.host,
            port=self.port,
            use_ssl=self.use_ssl,
            get_info=ALL,
            tls=self.tls,
            connect_timeout=self.connect_timeout
        )


class BuildServers:
    def __init__(self):
        self.servers = []
        self.hosts = Config.LDAP_HOSTS

    def build(self):
        if self.hosts:
            for host in self.hosts:
                self.servers.append(BuildServer(host[0], host[1]).build())
        else:
            self.servers.append(BuildServer(Config.LDAP_SERVER, Config.LDAP_PORT).build())
        return self.servers


class BuildFilter:
    def __init__(self):
        self.is_active_directory = Config.LDAP_IS_ACTIVE_DIRECTORY
        if self.is_active_directory:
            self.user_login_attr = Config.LDAP_USER_LOGIN_ATTR or "sAMAccountName"
            self.user_mail_attr = Config.LDAP_USER_MAIL_ATTR or "userPrincipalName"
        else:
            self.user_login_attr = Config.LDAP_USER_LOGIN_ATTR or "uid"
            self.user_mail_attr = Config.LDAP_USER_MAIL_ATTR or "mail"

        if self.user_login_attr == self.user_mail_attr:
            raise Exception(f"{Config.LDAP_USER_LOGIN_ATTR=} and {Config.LDAP_USER_MAIL_ATTR=} must be different")

    def build(self, user_login: str) -> str:
        search_filter = f"({self.user_login_attr}={user_login})"
        if Config.LDAP_ALLOW_USERNAME_OR_EMAIL_LOGIN:
            search_filter = f"(|{search_filter}({self.user_mail_attr}={user_login}))"
        return search_filter


class BuildAutoBind:
    def __init__(self):
        self.ldap3_auto_bind = {
            "PLAIN": AUTO_BIND_NO_TLS,
            "SIMPLE_TLS": AUTO_BIND_TLS_AFTER_BIND,
            "START_TLS": AUTO_BIND_TLS_BEFORE_BIND
        }
        self.ldap_encryption = Config.LDAP_ENCRYPTION.upper()

    def build(self):
        return self.ldap3_auto_bind[self.ldap_encryption]
